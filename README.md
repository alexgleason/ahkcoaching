# AHK Coaching SquareSpace template

Customized SquareSpace template for [ahkcoaching.com](https://ahkcoaching.com).
It's based on [*Ready*](https://ready-demo.squarespace.com/), a proprietary SquareSpace template.

## Local Development

See SquareSpace Developers' [*Local Development*](https://developers.squarespace.com/local-development) page.

## License

IANAL, so take this with a grain of salt.
See SquareSpace's [Developer Terms](https://www.squarespace.com/developer-terms), specifically *1.3 Template Source Code*.

Within the restrictions of SquareSpace's Developer Terms, consider my template modifications to be freely available to all for any purpose.

In short, you can probably use this code for your SquareSpace site, but that's it.
In any case, I won't be the one coming after you.
